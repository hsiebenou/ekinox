import streamlit as st
import pandas as pd
import numpy as np

# hidden ... bellow and on top page
st.markdown(
    """
<style>
.st-emotion-cache-iiif1v.ef3psqc3
{
    visibility: hidden;
}
.st-emotion-cache-h5rgaw.ea3mdgi1
{
    visibility: hidden;
}
</style>
""",
    unsafe_allow_html=True,
)
st.title(body="Test technique Ekinox ! ")
st.subheader(
    body="Ekinox conçoit pour les entreprises des produits numériques à très haute valeur ajoutée, notamment grâce à l'IA et au Big Data."
)
st.header("Contexte ")
st.markdown(
    " > Revenir dans le passé, en 2000 ! Et passer un deal super smart avec une boutique de vente de DVDs (vous vous rappelez encore ce que c'est on espère...) avec une promo qui déchire :\n"
    "- \tle dvd d'un volet de la saga vaut 15€\n"
    "- \tpour l'achat 2 volets DIFFÉRENTS de la saga, on applique une réduction de *10%* sur l'ensemble des DVDs **Back to the Future** achetés \n"
    "- \tpour l'achat de 3 volets DIFFÉRENTS de la saga, on applique une réduction de *20%* sur l'ensemble des DVDs **Back to the Future** achetés \n"
    "- \tLa boutique de DVDs vend également d'autres films qui coûtent chacun *20€*.\n"
)

# current_dir = os.path.abspath(os.path.dirname(__file__))
# table = pd.read_table(current_dir + "/tests/data/fichier_1.txt", header=None)
# table.rename(columns={0: "input"}, inplace=True)


def calculate_basket_price(dvds: list) -> str:
    """
    This function takes a list of DVDs as input and calculates the basket price.
    :param dvds list: list of dvds
    :return str: the price to pay
    """

    nb_back_to_the_future_dist = sum(
        film.startswith("Back to the Future") for film in np.unique(dvds)
    )
    nb_back_to_the_future = sum(film.startswith("Back to the Future") for film in dvds)

    if nb_back_to_the_future_dist >= 3:
        discount = 1 - 0.2
    elif nb_back_to_the_future_dist >= 2:
        discount = 1 - 0.1
    else:
        discount = 1
    cout_total = nb_back_to_the_future * 15 * discount
    if nb_back_to_the_future < len(dvds):
        cout_total += (len(dvds) - nb_back_to_the_future) * 20
    return f"Price : {cout_total} €"


uploaded_file = st.file_uploader("Choose the txt file to upload", type="txt")
if uploaded_file is not None:
    films = pd.read_table(uploaded_file, header=None)
    films.rename(columns={0: "input"}, inplace=True)
    st.table(films)
    films = list(films["input"].values)
    st.markdown(
        " > Please click on the **price to pay** button below to find out the price to pay !"
    )
    st.button(label="price to pay", key="price")
    if st.session_state.get("price"):
        st.text(calculate_basket_price(films))
else:
    st.warning(
        "Please download the file containing the dvds in txt format before continuing ..."
    )

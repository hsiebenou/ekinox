
**Objectif**  
Calculer le prix des DVDs en fonction du nombre et de la réduction

**Prérequis**:  
Pour exécuter cette application, nous avons besoin d'avoir:
- docker desktop installé sur son poste,
- un compte github pour récupérer les données et jouer les tests unitaires


**Initialisation et lancement de l'application**:  
- il faut cloner le repos git sur son poste en local
- il faut ouvrir docker desktop et s’assurer qu’il est en mode running
- il faut ouvrir une invite de commande (cmd: Windows / Terminal: Linux ou MacOs) et se placer à la racine du projet cloné


On peut déjà voir la liste des containers. Dans un terminal, jouez la commande suivante :
```
docker container ls -a
```


**Launch application**
Dans le terminal, lancer la ligne de commande suivante 
- construction de l'image
  ```
  docker image  build -t ekinox:1.0 .
  ```
- executer de l'application
  ```
  docker run -p 8501:8501 ekinox:1.0 
  ```
- aller sur un navigateur et taper cette url
  ```
  http://localhost:8501/
  ```

**Test unitaire**
- Les tests unitaires sont mis sur gitlab et permets de tester la fonction de calcul la remise ou non des prix
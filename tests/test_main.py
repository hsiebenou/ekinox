import pandas as pd
from main import calculate_basket_price


def test_read_and_calculated():
    dvds_1 = list(pd.read_table("tests/data/fichier_1.txt", header=None)[0])
    dvds_2 = list(pd.read_table("tests/data/fichier_2.txt", header=None)[0])
    dvds_3 = list(pd.read_table("tests/data/fichier_3.txt", header=None)[0])
    dvds_4 = list(pd.read_table("tests/data/fichier_4.txt", header=None)[0])
    dvds_5 = list(pd.read_table("tests/data/fichier_5.txt", header=None)[0])

    assert calculate_basket_price(dvds_1) == "Price : 36.0 €"
    assert calculate_basket_price(dvds_2) == "Price : 27.0 €"
    assert calculate_basket_price(dvds_3) == "Price : 15 €"
    assert calculate_basket_price(dvds_4) == "Price : 48.0 €"
    assert calculate_basket_price(dvds_5) == "Price : 56.0 €"
